const Course = require("../models/Course");

/*
Create a new course
Steps:
1. Create a new Course object
2. Save to the database
3. error handling
*/


module.exports.addCourse = (reqBody) => {
	console.log(reqBody);

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	//Save the created object to our database
	return newCourse.save().then((course, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

module.exports.addCourse = (reqBody) => {

	console.log(reqBody);

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {
		if (error) {
			return false;
		} else{
			return true;
		}
	})
}


//Retrieving All courses

module.exports.getAllCourses = () => {
	return Course.find({}).then( result => {
		return result;
	})
}

//Retrieve all Active courses

module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then(result => {
		return result;
	})
}


//Retrieve specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result;
	})
}

//Update a course
/*
Steps:
1. Create a variable which will contain the information retrieved from the request body
2. Find and update course using the course ID
*/
module.exports.updateCourse = (courseId, reqBody) => {
	//specify the properties of the doc to be updated 
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(courseId, updateCourse).then((course, error) =>{

		if(error) {
			return false;
		} else {
			return true;
		}
	})

}


//Archive a course

module.exports.archiveCourse = (reqParams) => {
	let updateActiveField = {
		isActive : false
	}

	return Course.findByIdAndUpdate(reqParams, updateActiveField).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}
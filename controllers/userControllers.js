const User = require("../models/User");

const Course = require("../models/Course");
//dependecy for encryoted password
const bcrypt = require("bcrypt");

const auth = require("../auth")
//Check if the email already exists


module.exports.checkemailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {

		if(result.length > 0) {
			return true
		} else {
			return false 
		}
	})
}


//user registration
/*Business Logic
1. Create a new  User object
2. Make sure that the password is encrypted
3. Save the new User to the database

*/ 

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		mobileNo : reqBody.mobileNo,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else 
		return true
	})
}


//User Authentication

/*
Steps:
1.  Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database.
3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	//findOne it will return the first record in the collection that matches the search criteria

	return User.findOne({ email: reqBody.email }).then(result => {
		if(result == null){
			return false;
		} else {


			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {

				return { accessToken : auth.createAccessToken(result.toObject()) }
			} else {
				return false;
			}
		}
	})
}


module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		
			result.password = "";
		
			return result;
		
	})
}


//Enroll user to a course
/*
Steps:
1. Find the document in the database using the user's ID
2. Add the courseID to the user's enrollment array using the push method.
3. Add the userId to the course's enrollees arrays
4. Save the document  
*/

module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then( user => {

		user.enrollments.push({ courseId: data.courseId});

		return user.save().then((user, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		})
	});


	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({ userId: data.userId });

		return course.save().then((course, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		})
	});


	//Validation 
	if(isUserUpdated && isCourseUpdated) {
		return true;
	} else {

		return false;
	}
};
const express = require("express");
const router = express.Router();
const CourseController = require("../controllers/courseControllers");


const auth = require('../auth')


router.post("/", (req, res) => {
	CourseController.addCourse(req.body).then(result => res.send(result));
})


router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	isAdmin = userData.isAdmin;

	console.log(isAdmin);

	if (isAdmin) {

	courseController.addCourse(req.body).then(result => res.send(result));
}	else{
	
	res.send(false);
}


});


//Retrieving all courses
router.get("/all", (req, res) => {
	CourseController.getAllCourses().then(result => res.send(result));
});


//Retrieving all ACTIVE courses
router.get("/", (req, res) => {
	CourseController.getAllActive().then(result => res.send(result))
});

//Retrieving a specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);

	CourseController.getCourse(req.params.courseId).then(result => res.send(result))
});


//Update a course

router.put("/:courseId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
})



router.put('/:courseId/archive', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		CourseController.archiveCourse(req.params.courseId).then(result => res.send(result))
	} else {
		res.send(false);
	}
})



module.exports = router;
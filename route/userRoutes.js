const express = require("express");
const router = express.Router();
const auth = require("../auth")

const UserController = require("../controllers/userControllers")

//Router for checking if the user's email already exists in the database

router.post("/checkEmail", (req, res) => {
	UserController.checkemailExists(req.body).then(result => res.send(result))
})


//Registration for user 
//http://localhost:4000/api/users/register
router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
})

//User Authentication(login)
router.post("/login", (req, res) => {
	UserController.loginUser(req.body).then(result => res.send(result));
})


router.post("/details", (req, res) => {
	UserController.getProfile(req.body).then(result => res.send(result));
})



//Enroll user to a course

router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId
	}

	UserController.enroll(data).then(result => res.send(result));
	
	
})







module.exports = router;


// /api/users/checkEmail